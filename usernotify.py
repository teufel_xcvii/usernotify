import time, os
logfilename = '/var/log/auth.log'
file = open(logfilename,'r')
st_results = os.stat(logfilename)
st_size = st_results[6]
file.seek(st_size)

while 1:
    where = file.tell()
    lastline = file.readline()
    if not lastline:
        time.sleep(1)
        file.seek(where)
    else:
        if "Accepted" in lastline:
            splitdata = lastline.split()
            user = splitdata[8]
            ip = splitdata[10]
            os.system('telegram-send " ⚠️ [ OpenSSH ] > User %s logged in from %s !"' % (user,ip))
        if "dropbear" in lastline:
            if "succeeded" in lastline:
                splitdata = lastline.split()
                user = splitdata[9]
                ip = splitdata[11]
                fuser = user.strip("'")
                fip = ip[0:3]
                if "127" in fip:
                    os.system('telegram-send " 🟢 [ Stunnel ] > %s connected !"' % (fuser))
                else:
                    os.system('telegram-send " 🟢 [ Dropbear ] > %s connected ! "' % (fuser))
            if "Exit" in lastline:
                if "): Disconnect" in lastline:
                    splitdata = lastline.split()
                    user = splitdata[6]
                    fuser = user.strip("():")
                    if fuser in ['before']:
                        user = splitdata[9]
                        fuser = user.strip("():',")
                        print('Bad username or password [ %s ] !' % (fuser))
                    else:
                        os.system('telegram-send " 🔴 [ Client ] > %s disconnected !"' % (fuser))
                        print(lastline)
                if "before auth:" in lastline:
                    print('A client dropped without saying bye.')
                    print(lastline)
                else:
                    print(lastline)